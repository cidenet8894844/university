CREATE DATABASE university;


USE university;


CREATE TABLE students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    career VARCHAR(100),
    years INT,
);


INSERT INTO estudiantes (nombre, apellido, fecha_nacimiento, correo_electronico, programa_estudio, año_ingreso, direccion)
VALUES
    ('Daniel Arango', 'Ingeniería Informática', 27),
    ('Laura Gómez', 'Administración de Empresas', 23);
