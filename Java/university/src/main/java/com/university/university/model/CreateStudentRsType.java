package com.university.university.model;

import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para CreateStudentRs_Type complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="CreateStudentRs_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlRootElement(name = "CreateStudentRs")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateStudentRs_Type", propOrder = {
        "description",
        "code"
})
public class CreateStudentRsType {

    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "Code")
    protected int code;

    /**
     * Obtiene el valor de la propiedad description.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtiene el valor de la propiedad code.
     *
     */
    public int getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     *
     */
    public void setCode(int value) {
        this.code = value;
    }

}

