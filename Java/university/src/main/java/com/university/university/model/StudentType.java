package com.university.university.model;


import javax.xml.bind.annotation.*;
@XmlRootElement(name = "CreateStudentRq")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Student_Type", propOrder = {
        "id",
        "name",
        "career",
        "years",
})
public class StudentType {

    @XmlElement(name = "Id", required = true)
    protected String id;
    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "Career", required = true)
    protected String career;
    @XmlElement(name = "Years")
    protected int years;

    public String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad nombre.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCareer() {
        return career;
    }

    /**
     * Define el valor de la propiedad carrera.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCareer(String value) {
        this.career = value;
    }

    /**
     * Obtiene el valor de la propiedad carrera.
     *
     */
    public int getYears() {
        return years;
    }

    /**
     * Define el valor de la propiedad edad.
     *
     */
    public void setYears(int value) {
        this.years = value;
    }

    /**
     * Obtiene el valor de la propiedad edad.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */

}
