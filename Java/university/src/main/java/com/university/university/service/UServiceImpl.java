package com.university.university.service;

import com.university.university.model.ConsultStudentRsType;
import com.university.university.model.CreateStudentRsType;
import com.university.university.model.StudentType;
import com.university.university.model.entity.UStudent;
import com.university.university.repository.URepo;
import com.university.university.utils.Converter;
import net.bytebuddy.dynamic.DynamicType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.Optional;


@Service
public class UServiceImpl implements UService {

    private URepo repo;
    private Converter converter;

    @Autowired
    public UServiceImpl(URepo repo, Converter converter) {
        this.repo = repo;
        this.converter = converter;
    }

    @Transactional(readOnly = true)
    @Override
    public ConsultStudentRsType findStudent(String id) {
        ConsultStudentRsType consultStudentRsType = new ConsultStudentRsType();
        //Converter converter = new Converter();
        Optional<UStudent> studentResult = repo.findById(id);
        if (studentResult.isPresent()) {
            UStudent uStudent = studentResult.get();
            StudentType studentRs = converter.toDto(uStudent);
            consultStudentRsType.setStudent(studentRs);
            consultStudentRsType.setCode("0");
            consultStudentRsType.setMessage("Encontrado");
        }
        else{
            consultStudentRsType.setCode("1");
            consultStudentRsType.setMessage("No encontrado");
        }
        return consultStudentRsType;
    }

    @Transactional
    @Override
    public CreateStudentRsType saveStudent(StudentType student) {
        CreateStudentRsType studentRs = new CreateStudentRsType();
        //Converter converter = new Converter();
        UStudent uStudent = converter.toEntity(student);

        uStudent = repo.save(uStudent);

        studentRs.setCode(0);
        studentRs.setDescription("Guardado");
        return studentRs;
    }


}