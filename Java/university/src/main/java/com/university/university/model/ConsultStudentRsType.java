package com.university.university.model;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "ConsultStudentRs")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultStudentRs_Type", propOrder = {
        "message",
        "code",
        "student"
})
public class ConsultStudentRsType {

    @XmlElement(name = "Message", required = true)
    protected String message;
    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Student")
    protected StudentType student;

    /**
     * Obtiene el valor de la propiedad mensaje.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessage() {
        return message;
    }

    /**
     * Define el valor de la propiedad mensaje.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Obtiene el valor de la propiedad codigo.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad codigo.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad paciente.
     *
     * @return
     *     possible object is
     *     {@link StudentType }
     *
     */
    public StudentType getStudent() {
        return student;
    }

    /**
     * Define el valor de la propiedad paciente.
     *
     * @param value
     *     allowed object is
     *     {@link StudentType }
     *
     */
    public void setStudent(StudentType value) {
        this.student = value;
    }

}
