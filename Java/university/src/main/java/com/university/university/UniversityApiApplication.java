package com.university.university;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversityApiApplication {

    public static void main(String[] args) {

        SpringApplication.run(UniversityApiApplication.class, args);
        System.out.println("Hello everyone!");
    }

}