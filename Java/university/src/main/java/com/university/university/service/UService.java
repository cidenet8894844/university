package com.university.university.service;

import com.university.university.model.ConsultStudentRsType;
import com.university.university.model.CreateStudentRsType;
import com.university.university.model.StudentType;


public interface UService {

    public ConsultStudentRsType findStudent(String id);

    public CreateStudentRsType saveStudent(StudentType student);


}
