package com.university.university.utils;

import com.university.university.model.StudentType;
import com.university.university.model.entity.UStudent;
import org.springframework.stereotype.Component;

@Component
public class Converter {

    public UStudent toEntity(StudentType student){
        UStudent vetStudent = new UStudent();
        UStudent.setId(student.getId());
        UStudent.setName(student.getName());
        UStudent.setCareer(student.getCareer());
        UStudent.setYears(student.getYears());
        return uStudent;
    }


    public StudentType toDto(UStudent uStudent){
        StudentType studentType = new StudentType();
        StudentType.setId(uStudent.getId());
        StudentType.setName(uStudent.getName());
        StudentType.setCareer(uStudent.getCareer());
        StudentType.setYears(uStudent.getYears());
        return studentType;
    }
}

