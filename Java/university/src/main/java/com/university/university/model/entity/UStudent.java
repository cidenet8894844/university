package com.university.university.model.entity;

import lombok.Data;
import org.springframework.data.domain.Persistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "students")
@Data
public class UStudent implements Persistable<String> {

    @Id
    @Column(name = "id")
    private String id;
    private String name;
    private String career;
    private Integer years;

    @Override
    public String getId() {
        return null;
    }

    @Override
    public boolean isNew() {
        return true;
    }
}

