package com.university.university.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateStudentRs_QNAME = new QName("http://university.osb.test", "CreateStudentRs");
    private final static QName _CreateStudentRq_QNAME = new QName("http://university.osb.test", "CreateStudentRq");
    private final static QName _ConsultStudentRq_QNAME = new QName("http://university.osb.test", "ConsultStudentRq");
    private final static QName _ConsultStudentRs_QNAME = new QName("http://university.osb.test", "ConsultStudentRs");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: university.osb.test
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateStudentRsType }
     *
     */
    public CreateStudentRsType createCreateStudentRsType() {
        return new CreateStudentRsType();
    }

    /**
     * Create an instance of {@link ConsultStudentRsType }
     *
     */
    public ConsultStudentRsType createConsultStudentRsType() {
        return new ConsultStudentRsType();
    }

    /**
     * Create an instance of {@link ConsultStudentRqType }
     *
     */
    public ConsultStudentRqType createConsultStudentRqType() {
        return new ConsultStudentRqType();
    }

    /**
     * Create an instance of {@link StudentType }
     *
     */
    public StudentType createStudentType() {
        return new StudentType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStudentRsType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://university.osb.test", name = "CreateStudentRs")
    public JAXBElement<CreateStudentRsType> createCreateStudentRs(CreateStudentRsType value) {
        return new JAXBElement<CreateStudentRsType>(_CreateStudentRs_QNAME, CreateStudentRsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StudentType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://university.osb.test", name = "CreateStudentRq")
    public JAXBElement<StudentType> createCreateStudentRq(StudentType value) {
        return new JAXBElement<StudentType>(_CreateStudentRq_QNAME, StudentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultStudentRqType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://university.osb.test", name = "ConsultStudentRq")
    public JAXBElement<ConsultStudentRqType> createConsultStudentRq(ConsultStudentRqType value) {
        return new JAXBElement<ConsultStudentRqType>(_ConsultStudentRq_QNAME, ConsultStudentRqType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultStudentRsType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://university.osb.test", name = "ConsultStudentRs")
    public JAXBElement<ConsultStudentRsType> createConsultStudentRs(ConsultStudentRsType value) {
        return new JAXBElement<ConsultStudentRsType>(_ConsultStudentRs_QNAME, ConsultStudentRsType.class, null, value);
    }

}

