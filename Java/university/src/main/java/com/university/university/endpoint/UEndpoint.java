package com.university.university.endpoint;

import com.university.university.model.ConsultStudentRqType;
import com.university.university.model.ConsultStudentRsType;
import com.university.university.model.CreateStudentRsType;
import com.university.university.model.StudentType;
import com.university.university.service.UService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class UEndpoint {

    private static final String NAMESPACE = "http://university.osb.test";

    private UService uService;

    @Autowired
    public UEndpoint(UService uService){
        this.uService = uService;
    }


    @PayloadRoot(namespace = NAMESPACE, localPart = "ConsultStudentRq")
    @ResponsePayload
    public ConsultStudentRsType getStudent(@RequestPayload ConsultStudentRqType studentRq){
        ConsultStudentRsType studentRs = new ConsultStudentRsType();

        try{
            studentRs = uService.findStudent(studentRq.getId());
        }catch (Exception e){
            System.out.println(e);
            studentRs.setCode("1");
            studentRs.setMessage("Error");
        }

        return studentRs;
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "CreateStudentRq")
    @ResponsePayload
    public CreateStudentRsType AddStudent(@RequestPayload StudentType studentRq){
        CreateStudentRsType studentRs = new CreateStudentRsType();

        try {
            studentRs = uService.saveStudent(studentRq);
        }catch (Exception e)
        {
            System.out.println(e);
            studentRs.setCode(1);
            studentRs.setDescription("Error, no se guardó el estudiante");
        }

        return studentRs;
    }
}