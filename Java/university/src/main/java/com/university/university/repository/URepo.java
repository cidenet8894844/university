package com.university.university.repository;

import com.university.university.model.entity.UStudent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface URepo extends CrudRepository<UStudent, String> {

}
