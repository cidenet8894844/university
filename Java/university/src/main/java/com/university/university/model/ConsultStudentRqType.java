package com.university.university.model;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "ConsultStudentRq")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultStudentRq_Type", propOrder = {
        "id"
})
public class ConsultStudentRqType {

    @XmlElement(name = "id", required = true)
    protected String id;

    /**
     * Obtiene el valor de la propiedad id.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setId(String value) {
        this.id = value;
    }

}
