xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://university.osb.test";
(:: import schema at "../Schemas/University.xsd" ::)

declare variable $output as element() (:: schema-element(ns1:ConsultStudentRs) ::) external;

declare function local:func($output as element() (:: schema-element(ns1:ConsultStudentRs) ::)) as element() (:: schema-element(ns1:ConsultStudentRs) ::) {
    <ns1:ConsultStudentRs>
        <ns1:Message>{fn:data($output/ns1:Message)}</ns1:Message>
        <ns1:Code>{fn:data($output/ns1:Code)}</ns1:Code>
        {
            for $student in $output/ns1:Student
            return
                <ns1:Student>
                    <ns1:Id>{fn:data($output/ns1:Student/ns1:Id)}</ns1:Id>
                    <ns1:Name>{fn:data($output/ns1:Student/ns1:Name)}</ns1:Name>
                    <ns1:Career>{fn:data($output/ns1:Student/ns1:Career)}</ns1:Career>
                    <ns1:Years>{fn:data($output/ns1:Student/ns1:Years)}</ns1:Years>
                </ns1:Student>
        }
    </ns1:ConsultStudentRs>
};

local:func($output)
