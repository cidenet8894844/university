xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://university.osb.test";
(:: import schema at "../WSDL/UniversityJava.wsdl" ::)

declare variable $input as element() (:: schema-element(ns1:ConsultStudentRq) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:ConsultStudentRq) ::)) as element() (:: schema-element(ns1:ConsultStudentRq) ::) {
    <ns1:ConsultStudentRq>
    {
      if ($input/ns1:Id) then
        <ns1:Id>{fn:data($input/ns1:Id)}</ns1:Id>
      else()
    }
    </ns1:ConsultStudentRq>
};

local:func($input)
