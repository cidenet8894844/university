xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://university.osb.test";
(:: import schema at "../Schemas/University.xsd" ::)

declare variable $output as element() (:: schema-element(ns1:CreateStudentRs) ::) external;

declare function local:func($output as element() (:: schema-element(ns1:CreateStudentRs) ::)) as element() (:: schema-element(ns1:CreateStudentRs) ::) {
    <ns1:CreateStudentRs>
        <ns1:Description>{fn:data($output/ns1:Description)}</ns1:Description>
        <ns1:Code>{fn:data($output/ns1:Code)}</ns1:Code>
    </ns1:CreateStudentRs>
};

local:func($output)
