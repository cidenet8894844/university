xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://university.osb.test";
(:: import schema at "../Schemas/University.xsd" ::)

declare variable $code as xs:string external;
declare variable $message as xs:string external;

declare function local:func($code as xs:string, 
                            $message as xs:string) 
                            as element() (:: schema-element(ns1:CreateStudentRs) ::) {
    <ns1:CreateStudentRs>
        <ns1:Description>{fn:data($message)}</ns1:Description>
        <ns1:Code>{fn:data($code)}</ns1:Code>
    </ns1:CreateStudentRs>
};

local:func($code, $message)
