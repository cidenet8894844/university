xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://university.osb.test";
(:: import schema at "../Schemas/University.xsd" ::)

declare variable $message as xs:string external;
declare variable $code as xs:string external;

declare function local:func($message as xs:string, 
                            $code as xs:string) 
                            as element() (:: schema-element(ns1:ConsultStudentRs) ::) {
    <ns1:ConsultStudentRs>
        <ns1:Message>{fn:data($message)}</ns1:Message>
        <ns1:Code>{fn:data($code)}</ns1:Code>
    </ns1:ConsultStudentRs>
};

local:func($message, $code)
