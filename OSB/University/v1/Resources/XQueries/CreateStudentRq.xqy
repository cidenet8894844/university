xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://university.osb.test";
(:: import schema at "../WSDL/UniversityJava.wsdl" ::)

declare variable $input as element() (:: schema-element(ns1:CreateStudentRq) ::) external;

declare function local:func($input as element() (:: schema-element(ns1:CreateStudentRq) ::)) as element() (:: schema-element(ns1:CreateStudentRq) ::) {
    <ns1:CreateStudentRq>
        <ns1:Id>{upper-case(fn:data($input/ns1:Id))}</ns1:Id>
        <ns1:Name>{upper-case(fn:data($input/ns1:Name))}</ns1:Name>
        <ns1:Career>{upper-case(fn:data($input/ns1:Career))}</ns1:Career>
        <ns1:Years>{fn:data($input/ns1:Years)}</ns1:Years>
    </ns1:CreateStudentRq>
};

local:func($input)
